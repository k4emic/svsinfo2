jQuery(document).ready(function($) {

    $('body').on('click', '.event-item', function() {
        
        var $elm = $(this).children('.collapse-container').first();
        var has_description = ($elm.children('.event-description').first().text().length > 0);
        var has_news = ($elm.children('.event-news-container').length > 0);
        
        if(!has_description && !has_news) {
            return false;
        }
        
        $('.collapse-container').not($elm).slideUp();
        
        $elm.slideDown();
        
    });
    
    function updateClock() {
        // show and update clock
        var now = new Date();
        var hours = formatTime(now.getHours());
        var minutes = formatTime(now.getMinutes());
        
        function formatTime(i) {
            if(i < 10) {
                i = "0" + i;
            }
            
            return i;
        }
        
        var timeString = hours + ":" + minutes;
        var elm_selector = '#clock';
        $(elm_selector).html(timeString);
    }
    
    updateClock();
    setInterval(updateClock, 500);
    
    var last_movement = 0;
    setInterval(idleHandler, 60 * 1000);
    
    $(this).mousemove(function(a, b) {
        last_movement = 0;
    });
    
    function idleHandler() {
        last_movement++;
        
        if(last_movement >= 2) {
            updateEventlist(window.location.href);
        }
    }

    function updateEventlist(location) {
        $.ajax(location, {
            success: function(data, textStatus, jqXhr) {
                var newContent = $(data);
                var newEventList = $('#index-container').replaceWith(newContent.find('#index-container'));
                updateClock(); // i'm lazy, sue me!
            }
        });
    }

    $('body').on('click', '.nav a', function(event) {
        event.preventDefault();
        updateEventList(this.href);
    });

    window.updateEventList = updateEventlist;
    
    $(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });
});
